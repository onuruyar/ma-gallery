import { terser } from 'rollup-plugin-terser'
import postcss from 'rollup-plugin-postcss'

module.exports = [
  {
    input: "src/gallery.js",
    output: {
      file: "public/gallery.min.js",
      format: 'iife'
    },
    plugins: [
      postcss({
        extract: true,
        minimize: true
      }),
      terser()
    ]
  },
  {
    input: "src/gallery-controls.js",
    output: {
      file: "public/gallery-controls.min.js",
      format: 'iife'
    },
    plugins: [
      postcss({
        extract: true,
        minimize: true
      }),
      terser()
    ]
  },
];