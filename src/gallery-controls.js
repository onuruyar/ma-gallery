import './gallery-controls.css'

customElements.define('ma-gallery-controls', class extends HTMLElement {
  constructor() {
    super()

    this._currentMedia = null
    this.handleEvent = this.handleEvent.bind(this)
    this.events = ['play', 'pause', 'volumechange', 'loadedmetadata', 'timeupdate']
  }

  connectedCallback() {
    this.addEventListener('click', this)
  }

  disconnectedCallback() {
    this.removeEventListener('click', this)
  }

  setup() {
    const { currentMedia } = this
    if (!currentMedia) return
    this.events.forEach(event =>
      currentMedia.addEventListener(event, this)
    )
    if (currentMedia.duration) {
      this.onloadedmetadata()
      this.ontimeupdate()
      this.onvolumechange()
      this.onplay()
    }
  }

  destroy() {
    const { currentMedia } = this
    if (!currentMedia) return
    this.events.forEach(event =>
      currentMedia.removeEventListener(event, this)
    )
  }

  handleEvent(e) {
    this.callHandler(`on${e.type}`, e)
  }

  callHandler(name, e) {
    const handler = this[name]
    if (handler && typeof handler === 'function') {
      handler.call(this, e)
    }
  }

  onclick(e) {
    const handlerName = e.target && e.target.dataset && e.target.dataset.handler
    if (handlerName) {
      e.preventDefault()
      this.callHandler(handlerName, e)
    }
  }

  onplay() {
    this.currentMedia && (this.dataset.paused = this.currentMedia.paused)
  }

  onpause() {
    this.currentMedia && (this.dataset.paused = this.currentMedia.paused)
  }

  onvolumechange() {
    this.dataset.muted = this.currentMedia.muted || this.currentMedia.volume === 0
    setTimeout(() => {
      if (this.volume !== this.currentMedia.volume) {
        this.volume = this.currentMedia.volume
      }
    }, 0)
  }

  onloadedmetadata() {
    this.duration = this.currentMedia.duration
    this.dataset.muted = this.currentMedia.muted
  }

  ontimeupdate() {
    this.currentTime = this.currentMedia.currentTime
  }

  playpause() {
    const { currentMedia } = this
    if (!currentMedia) return

    if (currentMedia.paused || currentMedia.ended) {
      currentMedia.play()
    } else {
      currentMedia.pause()
    }
  }

  mute() {
    const { currentMedia } = this
    if (!currentMedia) return
    currentMedia.muted = !currentMedia.muted
  }

  seek(e) {
    const { currentMedia } = this
    if (!currentMedia || !currentMedia.seekable || !currentMedia.duration) return
    const pos = (e.pageX  - e.target.offsetLeft) / e.target.offsetWidth
    currentMedia.currentTime = pos * currentMedia.duration
  }

  setVolume(e) {
    this.currentMedia.volume = e.target.value
  }

  set currentTime(currentTime) {
    const label = this.querySelector('label[data-bind="currentTime"]')
    if (label) {
      label.textContent = secondsToHms(currentTime)
    }

    const progress = this.querySelector('progress[data-bind="progress"]')
    if (progress) {
      progress.value = currentTime
    }
  }

  set duration(duration) {
    const label = this.querySelector('label[data-bind="duration"]')
    if (label) {
      label.textContent = secondsToHms(duration)
    }

    const progress = this.querySelector('progress[data-bind="progress"]')
    if (progress) {
      progress.setAttribute('min', 0)
      progress.setAttribute('max', duration)
    }
  }

  set volume(volume) {
    const input = this.querySelector('input[data-bind="volume"]')
    if (input) {
      input.value = volume
    }
  }

  set currentMedia(media) {
    if (media) {
      this._currentMedia = media
      this.setup()
    } else {
      this.destroy()
      this._currentMedia = media
    }
  }

  get currentMedia() {
    return this._currentMedia
  }
})

function secondsToHms(d) {
    d = Number(d)

    var h = Math.floor(d / 3600)
    var m = Math.floor(d % 3600 / 60)
    var s = Math.floor(d % 3600 % 60)

    return ((h > 0 ? h + ':' + (m < 10 ? '0' : '') : '0') + m + ':' + (s < 10 ? '0' : '') + s)
}