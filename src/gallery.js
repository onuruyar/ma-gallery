import './gallery.css'

customElements.define('ma-gallery-meta',
  class extends HTMLElement {
    constructor() {
      super()
      const template = document
        .querySelector('#ma-gallery-meta')
        .content
      const shadowRoot = this.attachShadow({mode: 'open'})
        .appendChild(template.cloneNode(true))
  }
})

const metaElement = document.createElement('ma-gallery-meta')
metaElement.classList.add('ma-gallery-view__meta')

const viewElement = document.createElement('div')
viewElement.classList.add('ma-gallery-view')

const imgElement = document.createElement('img')
imgElement.classList.add('ma-gallery-view__media')

customElements.define('ma-gallery', class extends HTMLElement {
  static get observedAttributes() {
    return ['options', 'items', 'src']
  }

  constructor() {
    super()

    this.direction = 'next'
    this.options = {autoplay: false, controls: true, muted: false, loop: false}
    this.index = 0
    this.items = []
    this.views = []
    this.media = null
    this.view = null

    const template = document
      .querySelector('#ma-gallery')
      .content
    const shadowRoot = this.attachShadow({mode: 'open'})
      .appendChild(template.cloneNode(true))
  }

  attributeChangedCallback(name, oldVal, newVal) {
    if (oldVal !== newVal) {
      this[name] = name !== 'src' ? JSON.parse(newVal) : newVal
    }
  }

  connectedCallback() {
    this.update()
    this.shadowRoot.addEventListener('click', this, { capture: true })
    this.shadowRoot.addEventListener('mousemove', this)
    this.shadowRoot.addEventListener('mouseover', this)
    window.addEventListener('keyup', this)
  }

  disconnectedCallback() {
    this.shadowRoot.removeEventListener('click', this)
    this.shadowRoot.removeEventListener('mousemove', this)
    this.shadowRoot.removeEventListener('mouseover', this)
    window.removeEventListener('keyup', this)

  }

  handleEvent(e) {
    const handler = this[`on${e.type}`] || this[e.target.dataset.handler]
    if (handler) {
      handler.call(this, e)
    }
  }

  onkeyup(e) {
    switch (e.code) {
      case 'ArrowRight': {
        this.next()
        break;
      }
      case 'ArrowLeft': {
        this.prev()
        break;
      }
      case 'Space': {
        this.media && this.media.play && (this.media.paused ? this.media.play() : this.media.pause())
        break;
      }
    }
  }

  onmousemove() {
    const { $nav, $controls, $meta } = this
    $nav && ($nav.dataset.active = true)
    $meta && ($meta.dataset.active = true)
    $controls && $controls.currentMedia && ($controls.dataset.active = true)

    if (this.activeInt) {
      clearTimeout(this.activeInt)
    }

    this.activeInt = setTimeout(() => {
      $nav && ($nav.dataset.active = false)
      $meta && ($meta.dataset.active = false)
      $controls && ($controls.dataset.active = false)
    }, this.options.uiActivationDelay || 3000)
  }
  onmouseover() {
    this.onmousemove()
  }

  next() {
    this.onmousemove()
    this.direction = 'next'
    this.leave()
    this.index = this.index < this.size ? this.index + 1 : 0
    this.update()
  }

  prev() {
    this.onmousemove()
    this.direction = 'prev'
    this.leave()
    this.index = this.index > 0 ? this.index - 1 : this.size
    this.update()
  }

  leave() {
    const { view, media, direction } = this

    if (media) {
      if (media.pause) {
        media.pause()
      }

      const leaveClassName = `ma-gallery-view-leave--${direction}`

      if (view.leaveHandler) {
        view.removeEventListener('transitionend', view.leaveHandler)
        view.leaveHandler = null
      }

      if (view.enterHandler) {
        removeEventListener('transitionend', view.enterHandler)
        view.enterHandler = null
      }

      view.classList.remove('ma-gallery-view-leave--next', 'ma-gallery-view-leave--prev')

      view.leaveHandler = (e) => {
        e.target.removeEventListener('transitionend', e.target.leaveHandler)
        e.target.leaveHandler = null
        e.target.classList.remove(leaveClassName)
        e.target.remove()
      }

      view.addEventListener('transitionend', view.leaveHandler, {once: true})
      view.classList.add(leaveClassName)
    }
  }

  update() {
    const { items, index, $frame, isSafari } = this
    const { autoplay, controls, muted, loop } = this.options
    const item = items[index]
    if (!item) return

    let view = this.views[index]
    let media

    if (!view) {
      view = viewElement.cloneNode(true)

      media = document.createElement(item.type)
      media.classList.add('ma-gallery-view__media')
      if (item.cover) media.classList.add('ma-gallery-view__media--cover')

      if (item.source) {
        item.source.forEach((source) => {
          const $source = document.createElement('source')
          Object.entries(source).forEach(([key, value]) => $source[key] = value)
          media.append($source)
        })
      }

      if (item.type === 'picture') {
        const img = imgElement.cloneNode(true)
        img.src = item.src
        if (item.cover) img.classList.add('ma-gallery-view__media--cover')
        media.append(img)
      } else {
        media.autoplay = 'autoplay' in item ? item.autoplay : autoplay
        media.loop = 'loop' in item ? item.loop : loop
        media.muted = 'muted' in item ? item.muted : (muted || (media.autoplay && isSafari))
        media.controls = controls === true
        media.preload = "auto"
        media.playsInline = true
        if (item.poster) {
          if (item.type === 'video') {
            media.poster = item.poster
          } else {
            const img = new Image
            img.src = item.poster
            img.classList.add('ma-gallery-view__poster')
            if (item.cover) img.classList.add('ma-gallery-view__poster--cover')
            view.append(img)
          }
        }
      }

      view.append(media)

      if (item.meta) {
        const meta = metaElement.cloneNode(true)
        Object.entries(item.meta).forEach(([name, content]) => {
          const el = document.createElement('span')
          el.slot = name
          el.textContent = content
          meta.append(el)
        })
        view.append(meta)
      }
    } else {
      media = view.querySelector('.ma-gallery-view__media')
    }

    if (controls === 'custom' && this.$controls) {
      const controlsElement = this.$controls
      setTimeout(() => {
      if (item.type === 'picture') {
        controlsElement.currentMedia = null
        controlsElement.dataset.active = false
      } else {
        controlsElement.currentMedia = media
        controlsElement.dataset.active = true
      }
      },0)
    }

    $frame.append(view)

    if (this.views[index]) {
      if (media.autoplay && media.play) {
        media.play()
      }
    } else {
      this.views[index] = view
    }

    this.media = media
    this.view = view

    this.enter()
  }

  enter() {
    const { view, direction } = this

    const enterClassName = `ma-gallery-view-enter--${direction}`

    if (view.leaveHandler) {
      view.removeEventListener('transitionend', view.leaveHandler)
      view.leaveHandler = null
    }

    view.classList.remove('ma-gallery-view-leave--next', 'ma-gallery-view-leave--prev')
    view.classList.add(enterClassName)

    if (view.enterHandler) {
      removeEventListener('transitionend', view.enterHandler)
      view.enterHandler = null
    }
    requestAnimationFrame(() => {
      view.enterHandler = (e) => {
        e.target.removeEventListener('transitionend', e.target.enterHandler)
        e.target.enterHandler = null
      }
      view.addEventListener('transitionend', view.enterHandler)
      requestAnimationFrame(() => view.classList.remove(enterClassName))
    })
  }

  get size() {
    return this.items.length - 1
  }

  get $frame() {
    return this.shadowRoot.querySelector('.ma-gallery-frame')
  }

  get $nav() {
    return this.shadowRoot.querySelector('.ma-gallery-nav')
  }

  get $meta() {
    return this.shadowRoot.querySelector('.ma-gallery-view__meta')
  }

  get $controls() {
    return this.shadowRoot.querySelector('ma-gallery-controls')
  }

  get isSafari() {
    return this._safari ||
      (
        this._safari =
          /safari/i.test(navigator.userAgent.toString()) &&
          !/chrome/i.test(navigator.userAgent.toString())
      )
  }

  set src(src) {
    fetch(src).then(r => r.json()).then(items => {
      this.items = items
      this.update()
    })
  }
})
